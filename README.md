
# `summarize`: compute summary statistics on the command line

This utility computes basic summary statistics for a list of numerical values and, optionally, prints them in JSON.
The output includes the following statistics:

- mean
- standard deviation
- minimum
- maximum
- median
- sum
- number of observations

If you have R installed, then you could do something similar with `Rscript`:
```
$ Rscript -e 'readLines("stdin") |> as.numeric() |> summary()' < values.txt
```
Indeed, if you like this solution, then it is not a bad idea to set an alias for it:
```
$ alias summarize="Rscript -e 'readLines(\"stdin\") |> as.numeric() |> summary()'"
$ summarize < values.txt
```

Why prefer the utility in this repository to `Rscript`?

- `Rscript` does not print the number of observations which would sometimes be useful.
- `Rscript` does not print the summary statistics in JSON which makes parsing the output harder.
- `Rscript` does not draw a histogram in the terminal.
- `Rscript` is also slightly slower but this is not an issue in practice.

## Installation

You can build `summarize` by running

```
$ git clone https://codeberg.org/gnyeki/summarize
$ cd summarize
$ make
```

To install the compiled executable in your user-specific `bin` directory, either `$HOME/.local/bin` or `$HOME/bin`, run

```
$ make install
```

If your `PATH` environment variable contains neither `$HOME/.local/bin` nor `$HOME/bin`, then `make install` will stop with an error.

## Examples

By itself, `summarize` prints all of the summary statistics in human-readable format:
```
$ echo 1 2 10 | summarize
Mean:               4.333333
Standard deviation: 4.932883
Median:             2.000000
Minimum:            1.000000
Maximum:            10.000000
Sum:                13.000000
Observations:       3
```

With the `--json` or `-j` command-line option, the output is changed to JSON:
```
$ echo 1 2 10 | summarize -j
{"mean": 4.333333, "sd": 4.932883, "median": 2.000000, "min": 1.000000, "max": 10.000000, "sum": 13.000000, "n": 3}
```

This becomes handy when combined with [`jq`](https://jqlang.github.io/jq/).
For example, to print just the mean:
```
$ echo 1 2 10 | summarize -j | jq .mean
4.333333
```

If invoked with `--histogram` or `-h`, `summarize` also draws a histogram in the terminal.
For example, this is an exponential distribution:
```
$ Rscript -e 'rexp(10000) |> cat()' | summarize -h

 █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ███░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ██████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ██████░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ████████░█░░░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ██████████░███░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ████████████████░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ██████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 █████████████████████░████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 █████████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 ██████████████████████████████████░░░██░░░░░░░░░░░░░░░░░░
 ███████████████████████████████████████░██░░░░░░░░░░░░░░░
 ████████████████████████████████████████████████████░█░░░
 █████████████████████████████████████████████████████████
 █████████████████████████████████████████████████████████

Mean:               0.986688
Standard deviation: 0.971703
Median:             0.691699
Minimum:            0.000131
Maximum:            7.898604
Sum:                9866.882492
Observations:       10000
```
