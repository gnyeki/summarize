CC = gcc
CFLAGS = -std=c99 -O2 -g -march=native -mtune=native -Wall -fstack-protector-strong

INSTALL_DIR := \
	$(shell \
		if (echo $(PATH) | tr ':' '\n' \
				| grep -q "^$(HOME)/[.]local/bin$$"); then \
			echo "$(HOME)/.local/bin"; \
		elif (echo $(PATH) | tr ':' '\n' \
				| grep -q "^$(HOME)/bin$$"); then \
			echo "$(HOME)/bin"; \
		else \
			echo "NOT_FOUND"; \
		fi)

.PHONY: build
build: build/summarize

build/version.h:
	[ -d build ] || mkdir build
	echo "const char* version = \"HEAD $(shell git rev-parse HEAD)\";" \
		> $@

build/summarize: src/main.c build/version.h
	[ -d build ] || mkdir build
	$(CC) $(CFLAGS) $< -Ibuild/ -o $@ -lm

.PHONY: install
install: build/summarize
	[ "$(INSTALL_DIR)" != "NOT_FOUND" ] \
		|| (echo "No user-specific bin directory in PATH."; exit 1)
	mkdir -p $(INSTALL_DIR)
	cp $< $(INSTALL_DIR)/
	chmod +x $(INSTALL_DIR)/$(notdir $<)

.PHONY: clean
clean:
	-rm -rf build/
