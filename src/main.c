#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "version.h"

typedef struct data {
  double* values;
  size_t length;
  size_t capacity;
} data_t;

typedef struct statistics {
  double mean;
  double sd;
  double median;
  double min;
  double max;
  double sum;
  size_t n;
} statistics_t;

#define MODE_JSON      1
#define MODE_HISTOGRAM 2

int parse_args(int argc, char** argv) {
  int mode = 0;
  size_t i;

  for (i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-j") == 0 || strcmp(argv[i], "--json") == 0) {
      mode |= MODE_JSON;
    } else if (strcmp(argv[i], "-h") == 0
               || strcmp(argv[i], "--histogram") == 0) {
      mode |= MODE_HISTOGRAM;
    } else {
      return -1;
    }
  }

  return mode;
}

/*
 * Add `value` to `data`.  Allocates more memory if we are at capacity.
 */
int add_value(data_t* data, double value) {
  double* old_values;
  double* new_values;
  size_t new_capacity;

  assert(data->length <= data->capacity);

  if (data->length == data->capacity) {
    /* Increase capacity by 50 percent. */

    old_values = data->values;

    new_capacity = (size_t)((double)data->capacity * 1.5);
    new_values = malloc(sizeof(*data->values) * new_capacity);

    memcpy(new_values, old_values, sizeof(*old_values) * data->length);

    data->values = new_values;
    data->capacity = new_capacity;

    free(old_values);
  }

  data->values[data->length++] = value;

  return 0;
}

/*
 * Three-way comparison function for `qsort`.
 */
int compare_doubles(const void* a, const void* b) {
  if (*(double*)a < *(double*)b) {
    return -1;
  } else if (*(double*)a > *(double*)b) {
    return 1;
  } else {
    return 0;
  }
}

/*
 * Calculate summary statistics.  Allocates memory for the return value.
 */
statistics_t* summarize(data_t* sorted_data) {
  statistics_t* result;
  double* values;
  size_t i, length;

  values = sorted_data->values;
  length = sorted_data->length;

  /* Compile statistics. */

  result = malloc(sizeof(statistics_t));

  result->min = values[0];
  result->max = values[length - 1];
  result->n = length;

  if (length % 2 == 1) {
    result->median = values[length / 2];
  } else {
    result->median =
      values[length / 2 - 1] / 2
      + values[length / 2] / 2;
  }

  result->mean = 0;
  result->sum = 0;

  for (i = 0; i < length; i++) {
    result->mean += values[i] / (double)length;
    result->sum += values[i];
  }

  result->sd = 0;

  if (length > 1) {
    for (i = 0; i < length; i++) {
      result->sd +=
        powl(values[i] - result->mean, 2)
        / (double)(length - 1);
    }

    result->sd = sqrt(result->sd);
  }

  return result;
}

void draw_histogram(data_t* sorted_data) {
  struct winsize window_size;
  unsigned short int max_height, n_bins;
  double *values, *bin_bounds;
  double p10, p90, bin_width, max_freq;
  size_t *freqs;
  size_t i, j, length;

  ioctl(STDOUT_FILENO, TIOCGWINSZ, &window_size);

  if (window_size.ws_col < 4 || window_size.ws_row < 12) {
    fprintf(stderr,
            "Warning: Terminal too small; not drawing histogram.\n");
    return;
  }

  values = sorted_data->values;
  length = sorted_data->length;

  /* Determine bin size and the bin count. */

  n_bins = window_size.ws_col - 2;
  max_height = window_size.ws_row - 10;

  p10 = values[length / 10];
  p90 = values[length * 9 / 10];

  bin_width = (p90 - p10) / (double)n_bins;

  freqs = malloc(sizeof(*freqs) * n_bins);

  /* Initialize bins. */
  for (i = 0; i < n_bins; i++) {
    freqs[i] = 0;
  }

  bin_bounds = malloc(sizeof(*bin_bounds) * n_bins);

  /* Find upper bound for each bin. */
  for (i = 0; i < n_bins; i++) {
    bin_bounds[i] = values[length / 10] + bin_width * (1 + i);
  }

  /* Count observations in each bin. */
  for (i = length / 10, j = 0; i < length * 9 / 10; i++) {
    while (values[i] > bin_bounds[j]) {
      j++;
    }

    freqs[j] += 1;
  }

  /* Find maximum frequency. */

  max_freq = 0;

  for (i = 0; i < n_bins; i++) {
    if (freqs[i] > max_freq) {
      max_freq = freqs[i];
    }
  }

  /* Draw histogram.  ░░ ▓▓ ██ */

  printf("\n");

  for (i = max_height; i > 0; i--) {
    /* Print row. */

    printf(" ");

    for (j = 0; j < n_bins; j++) {
      if (freqs[j] > max_freq / max_height * (i - 1)) {
        printf("█");
      } else {
        printf("░");
      }
    }

    printf("\n");
  }

  printf("\n");

  free(bin_bounds);
  free(freqs);
}

int main(int argc, char** argv) {
  data_t* data;
  statistics_t* stats;
  double this_value;
  int mode;

  mode = parse_args(argc, argv);

  if (mode < 0 || (mode & MODE_JSON && mode & MODE_HISTOGRAM)) {
    fprintf(stderr, "Error: Invalid arguments.\n");
    fprintf(stderr, "Usage: %s [-j|--json]\n", argv[0]);
    fprintf(stderr, "   or: %s [-h|--histogram]\n", argv[0]);
    return 1;
  }

  data = malloc(sizeof(data_t));
  data->length = 0;
  data->capacity = 100;
  data->values = malloc(sizeof(*data->values) * data->capacity);

  while (scanf("%lf", &this_value) > 0) {
    add_value(data, this_value);
  }

  /* Sort the values for `summarize` and `draw_histogram`. */
  qsort(data->values,
        data->length,
        sizeof(*data->values),
        compare_doubles);

  stats = summarize(data);

  if (mode & MODE_JSON) {
    printf("{\"mean\": %lf, ", stats->mean);
    printf("\"sd\": %lf, ", stats->sd);
    printf("\"median\": %lf, ", stats->median);
    printf("\"min\": %lf, ", stats->min);
    printf("\"max\": %lf, ", stats->max);
    printf("\"sum\": %lf, ", stats->sum);
    printf("\"n\": %ld}\n", stats->n);
  } else {
    if (mode & MODE_HISTOGRAM) {
      draw_histogram(data);
    }

    printf("Mean:               %lf\n", stats->mean);
    printf("Standard deviation: %lf\n", stats->sd);
    printf("Median:             %lf\n", stats->median);
    printf("Minimum:            %lf\n", stats->min);
    printf("Maximum:            %lf\n", stats->max);
    printf("Sum:                %lf\n", stats->sum);
    printf("Observations:       %ld\n", stats->n);
  }

  free(stats);
  free(data->values);
  free(data);

  return 0;
}
